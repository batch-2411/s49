// Mock Database
// let posts = [];

// Post ID
// let count = 1;

fetch('https://jsonplaceholder.typicode.com/posts')
	.then(response => response.json())
	.then(data => showPosts(data));

// Reactive DOM with JSON (CRUD Operation)

// ADD POST DATA

document.querySelector('#form-add-post').addEventListener('submit', (e) => {
	e.preventDefault();
	fetch('https://jsonplaceholder.typicode.com/posts', {
		method: 'POST',
		body: JSON.stringify({
			title: document.querySelector('#txt-title').value,
			body: document.querySelector('#txt-body').value,
			userId: 1
		}),
		headers: {
			'Content-type': 'application/json; charset=UTF-8'
		}
	})
	.then(response => response.json())
	.then(data => {
		console.log(data);
		alert('Post successfully added');
		document.querySelector('#txt-title').value = null;
		document.querySelector('#txt-body').value = null;
	});
});

// RETRIEVE POST
const showPosts = (posts) => {
	let postEntries = "";

	posts.forEach(post => {
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onClick="editPost('${post.id}')">Edit</button>
				<button onClick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	});
	console.log(postEntries);
	document.querySelector('#div-post-entries').innerHTML = postEntries;
};

// EDIT POST (Edit Button)
const editPost = (id) => {
	// The function first uses the querySelector() method to get the element with the id "#post-title-${id}" and "#post-body-${id}" and assigns its innerHTML property to the title varaibale with the same body
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;

	document.querySelector('#btn-submit-update').removeAttribute('disabled');
};

// UPDATE POST (Update Button)
document.querySelector('#form-edit-post').addEventListener('submit', e => {
	e.preventDefault();
	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: 'PUT',
		body: JSON.stringify({
			id: document.querySelector('#txt-edit-id').value,
			title: document.querySelector('#txt-edit-title').value,
			body: document.querySelector('#txt-edit-body').value,
			userId: 1
		}),
		headers: {'Content-type': 'application/json; charset=UTF-8'}
	})
	.then(response => response.json())
	.then(data => {
		console.log(data);
		alert('Post successfully updated!');
		// Reset the edit post form input fields
		document.querySelector('#txt-edit-id').value = null;
		document.querySelector('#txt-edit-title').value = null;
		document.querySelector('#txt-edit-body').value = null;
		// setAttribute() - adds/sets an attribute to an HTML element
		document.querySelector('#btn-submit-update').setAttribute('disabled', true);
	});
});

// DELETE POST (Delete Button)
const deletePost = (id) => {
	fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
		method: 'DELLETE',
		headers: {'Content-type': 'application/json; charset=UTF-8'}
	})
	.then(response => response.json())
	.then(data => console.log(data))

	
	document.querySelector(`#post-${id}`).remove();
}